/**
  *
  * Beschreibung
  *  Einf�hrung in die Programmierung mit Arrays 
  * @version 1.0 vom 04.05.2022
  * @author Martin Kr�ger 
  */
import java.lang.reflect.Array;
import java.util.Scanner;
public class arrayEinfuehrung {
  
  public static void main(String[] args) {
    
    Scanner sc = new Scanner (System.in);
    
    //Deklaration eines Arrays, 5 integer Werte
    //Name des Arrays: "intArray"
    int groesse = 5;
    int [] intArray = new int [groesse];
    
    //Speichern von Werten im Array
    //Wert 1000 an 3. Stelle des Arrays
    //Wert 500 an Index 4
    
    intArray[2] = 1000;
    intArray[4]	= 500;
    
    //Deklaration + Initialisierung eines Arrays in einem Schritt
    //Name des Arrays: "doubleArray"
    // L�nge 3, mit 3 double Werten 
    
    double [] doubleArray = {1.1,2.2,3.3};
    
    
    //Der Wert des double Arrays an Indexposition 1 soll ausgegeben werden
    
    System.out.println(doubleArray[1]);
    
    //Nutzer soll einen Index angeben 
    //und an diesen Index einen neuen Wert speichern
    System.out.println("An welchen Index soll der neue Wert?");
    int index = sc.nextInt();
    
    System.out.println("Geben Sie den neuen Wert f�r index " + index + " an:");
    
    doubleArray[index] = sc.nextDouble();
    
    //Alle Werte vom Array intArray sollen ausgegeben werden
    //Geben Sie zun�chst an, welche Werte Sie in der Ausgabe erwarten: 
    // _0_   _0_  _1000_  _0_  _500_  
    
    for (int ausgabe = 0; ausgabe < groesse; ausgabe++) {
    System.out.print(intArray[ausgabe] + " ");
    }
    
    System.out.println("");
    //Der intArray soll mit neuen Werten gef�llt werden
          //1. alle Felder sollen den Wert 0 erhalten
    int reset = 0;
    for (int fuellung = 0; fuellung < groesse; fuellung++) {
    	
    	intArray[fuellung] = reset;
    	System.out.print(intArray[fuellung]+" ");
    }
    
    System.out.println("");
    //Der intArray soll mit neuen Werten gef�llt werden
          //2. alle Felder sollen vom Nutzer einen Wert bekommen
          //nutzen Sie dieses Mal die Funktion �length�
    
    for( int fuellung = 0; fuellung < intArray.length; fuellung++) {
    	
    	System.out.println("Wert f�r den Index "+ fuellung);
    	intArray [fuellung] = sc.nextInt();
    	}
    
    for(int ausgabe = 0; ausgabe < intArray.length; ausgabe++) {
    	
    	System.out.print(intArray[ausgabe]+" ");
    }
    System.out.println("");
    
    //Der intArray soll mit neuen Werten gef�llt werden
           //3. Felder automatisch mit folgenden Zahlen f�llen: 10,20,30,40,50
    int wert = 10;
for( int fuellung = 0; fuellung < intArray.length; fuellung++) {    
   
	intArray[fuellung] = wert;
	System.out.print(intArray[fuellung] + " ");
	wert = wert + 10;
	}
    
    
  } // end of main
  
} // end of class arrayEinfuehrung
