import java.util.Scanner;
//Scanner Klasse muss importiert werden, um Nutzereingaben einlesen zu k�nnen 

public class KleinerRechner2 {
	
	public static void addition(double zahl1, double zahl2) {
		double ergebnis = zahl1 + zahl2; 
    	System.out.printf("Das Ergebnis der Addition von " + zahl1 + " + " +zahl2+ " ist gerundet: %.2f\n" , ergebnis);
		
	}
	public static void subtraktion (double zahl1, double zahl2) {
		double ergebnis = zahl1 - zahl2; 
    	System.out.printf("Das Ergebnis der Subtraktion von " + zahl1 + " - " +zahl2+ " ist gerundet: %.2f\n" , ergebnis);
		
	}
	public static void multiplikation (double zahl1, double zahl2) {
		double ergebnis = zahl1 * zahl2; 
    	System.out.printf("Das Ergebnis der Multiplikation von " + zahl1 + " * " +zahl2+ " ist gerundet: %.2f\n" , ergebnis);
		
	}
	public static void division (double zahl1, double zahl2) {
		double ergebnis = zahl1 / zahl2; 
    	System.out.printf("Das Ergebnis der Division von " + zahl1 + " / " +zahl2+ " ist gerundet: %.2f\n" , ergebnis);
		
	}
	public static double eingabe(Scanner sc,String text) {
		
	
		System.out.println("Geben Sie die "+ text +" ein:");	
		return sc.nextDouble();
	}
  
  public static void main(String[] args) {
    
    //wir erstellen einen eigenen Scanner, Namens "sc "
    //der Name kann beliebig gew�hlt werden
    Scanner sc = new Scanner (System.in);
    int konversation = 1,menuewahl,ende;
    String wie;
    int zahl1=1;
    int zahl2=2;
    
    do {
    	System.out.println("Sch�nen Guten Tag, wie kann ich Ihnen helfen? (garnicht/Taschenrechner/Gespr�ch)");
    	wie = sc.nextLine();
    if (wie.equals("garnicht")) {
    	System.out.println("Bis zum n�chsten mal");
    	konversation = 0;
    	
    } else if(wie.equals("Taschenrechner")) {
    
    		do {
    			
    			do {
    				System.out.println("W�hle deine Rechenart aus:");
    			
    			System.out.println("1 Addidtion\n2 Subtraktion\n3 Multiplikation \n4 Division");
    			menuewahl =sc.nextInt();
    			
    			switch (menuewahl){
    			case 1: 
    				System.out.println("Sie haben Addition gew�hlt:\n");
    			addition(eingabe(sc, "1. Zahl"),eingabe(sc, "2. Zahl"));
    	    	break;
    			case 2: 
    				System.out.println("Sie haben Subtraktion gew�hlt:\n");
    	    	subtraktion(eingabe(sc, "1. Zahl"),eingabe(sc, "2. Zahl"));
    	    	break;
    			case 3: 
    				System.out.println("Sie haben Multiplikation gew�hlt:\n");
    			multiplikation(eingabe(sc, "1. Zahl"),eingabe(sc, "2. Zahl"));
    	    	break;
    			case 4:
    				System.out.println("Sie haben Division gew�hlt:\n");
    			division(eingabe(sc, "1. Zahl"),eingabe(sc, "2. Zahl"));
    	    	break;
    	    	default: 
    	    	System.out.println("Falsche Eingabe\n");
    	    	menuewahl = 100;
    			}
    			} while(menuewahl == 100);
    	   	    
    			System.out.println("\nNochmal?\n1 ja\n2 nein");
    			ende = sc.nextInt();
    	  
    	    	} while (ende == 1 );
    	    
    	    	System.out.println("Schade, bis n�chstes mal!");
    	    	konversation = 0;
    } else if (wie.equals("Gespr�ch")) {
    	
    	System.out.println("Das wird sp�ter hinzugef�gt....");
    	
    } else {
    	
    	System.out.println("FEHLER");
    }
   
    	
     } while (konversation == 1);
    
    
    
    
    
    
    
    
   
   sc.close(); 
  }
 }
