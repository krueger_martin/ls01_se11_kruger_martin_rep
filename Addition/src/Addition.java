import java.util.Scanner;

public class Addition {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        double zahl1, zahl2, erg;

        
        programmhinweis();															//Methodenaufruf																
        
        zahl1 = eingabe(1);
      	zahl2 = eingabe(2);
 
      	erg = verarbeitung(zahl1,zahl2);
        
        ausgabe(zahl1, zahl2, erg);													//Argumente (zahl1, zahl2, erg)
        
   
        
    }
    
    	public static void programmhinweis() {										//Methodenname Programmhinweis
    		System.out.println("Hinweis: ");
    		System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
    	
    	}
    	
    	public static void ausgabe(double zahl1,double zahl2,double erg) {			//Parameter (zahl1,zahl2,erg) mit Datentyp angegeben						
    					
    		System.out.println("Ergebnis der Addition");							
    		System.out.printf("%.2f = %.2f + %.2f", erg, zahl1, zahl2);
    	}
    	
    	public static double verarbeitung(double zahl1,double zahl2) {				//Methodenkopf	Methode wird im Kopf definiert	37				
    	
    		double erg;																//Methodenrumpf (alles zwischen geschweiften Klammern)
    		erg = zahl1 + zahl2;
    																				//Methodendefinition (gesamte Methode) 37 - 43
    		return erg; //return - R�ckgabewert
    	}
    	
    	public static double eingabe(int nummer) {									//double - R�ckgabedatentyp							
    		double zahl;														// zahl - lokale Variable
        	System.out.println(nummer + ". Zahl:");
        	zahl = sc.nextDouble();
        	return zahl;
    	
    	}
  // public - Zugriffsmodifizierer
  // void - kein R�ckgabetyp (leer)
  // main - Methodenname
}