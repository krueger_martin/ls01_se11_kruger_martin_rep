
public class VariablenVerwenden {



/** Variablen.java
	    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
	    @author MartinKr�ger
	    @version 1
	*/
public static void main(String [] args){
	    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
	          Vereinbaren Sie eine geeignete Variable */
				
			int programmdurchlauf;

	    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
	          und geben Sie ihn auf dem Bildschirm aus.*/
				
				short zaehler = 25;
				System.out.println("Wert des Z�hlers: " + zaehler);
				
	    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
	          eines Programms ausgewaehlt werden.
	          Vereinbaren Sie eine geeignete Variable */
				
				byte menuepunkt;

	    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
	          und geben Sie ihn auf dem Bildschirm aus.*/
				
				char buchstabe = 'C';

	    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
	          notwendig.
	          Vereinbaren Sie eine geeignete Variable */
				
				long grosseZahlenwerte;

	    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
	          und geben Sie sie auf dem Bildschirm aus.*/
				
				int lichtgeschwms = 299792458;

	    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
	          soll die Anzahl der Mitglieder erfasst werden.
	          Vereinbaren Sie eine geeignete Variable und initialisieren sie
	          diese sinnvoll.*/
				
				short anzahlMitglieder = 7;

	    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
				
				System.out.println("Anzahl der Mitgleider: " + anzahlMitglieder);

	    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
	          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
	          dem Bildschirm aus.*/

				final double elementarLadung = 1.602E-19;
				System.out.println("Die Elementarladung e = " + elementarLadung);
				
		 /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
	          Vereinbaren Sie eine geeignete Variable. */

				boolean zahlungerfolgt;
				
	    /*11. Die Zahlung ist erfolgt.
	          Weisen Sie der Variable den entsprechenden Wert zu
	          und geben Sie die Variable auf dem Bildschirm aus.*/	
				
				zahlungerfolgt = true;
				System.out.println("Ist die Zahlung erfolgt? : " + zahlungerfolgt);
				
				
				
				
				
	  }//main
	}// Variablen

