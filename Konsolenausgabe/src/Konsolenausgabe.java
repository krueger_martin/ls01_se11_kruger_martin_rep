
public class Konsolenausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	//Aufgabe 1 
		System.out.println("Aufgabe 1:\n"); //Abstandshalter der Aufgaben
		
		
	System.out.print("Ich mag \"programmieren\".\n");
	System.out.print("Manchmal.");
	
	//print = ausgeben
	//println = ausgeben und dann neue Zeile.
	
		System.out.println("\n");			//Abstandshalter der Aufgaben
	//Aufgabe 2
		System.out.println("Aufgabe 2:\n");//Abstandshalter der Aufgaben
			
		
	System.out.printf("%7s\n", "*");
	System.out.printf("%8s\n", "***");
	System.out.printf("%9s\n", "*****");
	System.out.printf("%10s\n", "*******");
	System.out.printf("%11s\n", "*********");
	System.out.printf("%12s\n", "***********");
	System.out.printf("%13s\n", "*************");
	System.out.printf("%8s\n", "***");
	System.out.printf("%8s\n", "***");
	 
		System.out.print("\n");				//Abstandshalter der Aufgaben
	//Aufgabe 3
		System.out.println("Aufgabe 3:\n"); //Abstandshalter der Aufgaben
		 			
	
		
	/*	22.4234234			=> 22,42
		111.2222			=>111,22
		4.0					=>4,00
		1000000.551			=>1000000,55
		97.34				=>97,34			*/
	
	System.out.printf("%.2f\n",22.4234234);
	System.out.printf("%.2f\n",111.2222);
	System.out.printf("%.2f\n",4.0);
	System.out.printf("%.2f\n", 1000000.551);
	System.out.printf("%.2f\n", 97.34);
	
	
	}

}
